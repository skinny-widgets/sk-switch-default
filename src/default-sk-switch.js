
import { SkSwitchImpl } from '../../sk-switch/src/impl/sk-switch-impl.js';

export class DefaultSkSwitch extends SkSwitchImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'switch';
    }

    get inputEl() {
        if (! this._inputEl) {
            this._inputEl = this.comp.el.querySelector('input');
        }
        return this._inputEl;
    }

    set inputEl(el) {
        this._inputEl = el;
    }

    get subEls() {
        return [ 'inputEl' ];
    }

    bindEvents() {
        super.bindEvents();
        this.clickHandle = this.onClick.bind(this);
        this.comp.onclick = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onClick(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
    }

    unbindEvents() {
        super.unbindEvents();
        this.comp.onclick = null;
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }

    enable() {
    }

    disable() {
    }

    check() {
        super.check();
        this.inputEl.setAttribute('checked', 'checked');
    }

    uncheck() {
        super.check();
        this.inputEl.removeAttribute('checked');
    }
}
